﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FinaleScreen : MonoBehaviour
{
	//background is solid colour

	//single button centred which will be angled correctly to portrait or landscape
	//click = restart app

	public Image background;
	public Button button;

	private void Start()
	{
		SetBackgroundColour();
	}

	void SetBackgroundColour()
	{
#if UNITY_EDITOR
		background.color = Color.yellow;
#endif

#if UNITY_ANDROID
		background.color = Color.red;
#endif

#if UNITY_IOS
		background.color = Color.Green;
#endif
	}

	public void Restart()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}
}