﻿namespace Developer.Screen
{
    using UnityEngine;
	using Test;
    using System.Threading.Tasks;
    using System.Collections;
    using UnityEngine.UI;

    /// <inheritdoc />
    /// <summary>
    /// Controller for the screen that displays a <see cref="Developer.Component.TrafficLight" />.
    /// </summary>
    public sealed class LightScreen : Core.ScreenAbstract
    {
        private Developer.Component.TrafficLight lights = null;

		Text textCounter;

		private void Awake()
		{
			//Spawn Traffic Lights
			GameObject trafficLightPrefab = Resources.Load<GameObject>("Prefabs/Component/TrafficLight/TrafficLight");
			GameObject trafficLightInstance = Instantiate(trafficLightPrefab, transform.Find("TrafficLightHolder"));
			lights = trafficLightInstance.AddComponent<Developer.Component.TrafficLight>();
			lights.SetState();

			//Spawn Finale Screen
			GameObject finaleScreen = Instantiate(Resources.Load<GameObject>("Prefabs/Screen/FinaleScreen"), transform.Find("TrafficLightHolder").parent);
			FinaleScreen fScreen = finaleScreen.GetComponent<FinaleScreen>();
			fScreen.background = trafficLightInstance.GetComponent<Image>();
			fScreen.button = finaleScreen.GetComponentInChildren<Button>();
			fScreen.button.onClick.AddListener(delegate { fScreen.Restart(); } );

			//Create Countdown Timer
			CreateText();

			//Get Light Values
			_ = GetLightValue();
		}

		async Task GetLightValue()
		{
			//loop 100 times
			for(int x = 0; x < 100; x++)
			{
				//get result
				Task<int> res = GetData();

				//300ms timer
				for(int t = 0; t < 300; t++)
				{
					float time = 3 - (t / 100f);
					textCounter.text = time.ToString("F1");
					await Task.Delay(1);
				}

				//check result 3,5,both
				//get result
				int result = res.Result;
				int three = result % 3;
				int five = result % 5;
				
				//if multiple of 3 and 5 set green
				if(three == 0 && five == 0)
				{
					lights.SetState(LightColor.Green);
					print(result + " is divisible by 3 and 5!");
				}
				//if multiple of 5 set yellow
				else if(five == 0)
				{
					lights.SetState(LightColor.Yellow);
					print(result + " is divisible by 5!");
				}
				//if multiple of 3 set red
				else if(three == 0)
				{
					lights.SetState(LightColor.Red);
					print(result + " is divisible by 3!");
				}
				//if none
				else
				{
					lights.SetState();
					print(result + " isn't divisible by 3 or 5!");
				}
			}
		}

		async Task<int> GetData()
		{
			int result = await NetworkService.GetCurrentLightValue();
			return result;
		}

		void CreateText()
		{
			//find canvas
			GameObject canvas = GameObject.Find("Canvas");

			//create text and apply parent
			GameObject text = new GameObject("Counter");
			text.transform.SetParent(canvas.transform);

			//add text component 
			textCounter = text.AddComponent<Text>();

			//set font
			textCounter.font = Resources.GetBuiltinResource<Font>("Arial.ttf");

			//set best fit
			textCounter.resizeTextForBestFit = true;
			textCounter.resizeTextMinSize = 1;
			textCounter.resizeTextMaxSize = 100;

			//set allignment
			textCounter.alignment = TextAnchor.MiddleCenter;

			//set anchor
			RectTransform rect = textCounter.GetComponent<RectTransform>();
			rect.anchorMin = new Vector2(0, 1);
			rect.anchorMax = new Vector2(0, 1);
			rect.anchoredPosition = new Vector3(150, -150, 0);

			//set size
			rect.sizeDelta = new Vector2(300, 300);

			//set scale
			rect.localScale = Vector3.one;
		}
	}
}