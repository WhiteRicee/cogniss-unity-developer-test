﻿namespace Developer.Core
{
	using UnityEngine;

    /// <inheritdoc />
    /// <summary>
    /// Controller which loads all required plugins before loading the first screen for the app.
    /// </summary>
    public sealed class PlatformLauncher : MonoBehaviour
    {
        private const bool UseReporter = true;

        private App _app = null;

        private void Awake()
        {
			if(UseReporter)
			{
				if(!FindObjectOfType<Reporter>())
				{
					GameObject reporter = new GameObject("Reporter");
					reporter.AddComponent<Reporter>();
				}
			}

            _app = new App(GameObject.Find("Canvas"));
            _app.ChangeScreen<Developer.Screen.LoadingScreen>();
        }
    }
}