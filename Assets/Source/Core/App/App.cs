﻿namespace Developer.Core
{
    using UnityEngine;

    /// <summary>
    /// Controller for the application as a whole.
    /// </summary>
    public sealed class App
    {
        private readonly GameObject _canvas = null;
        private ScreenAbstract currentScreen = null;

        /// <summary>
        /// Creates a new App container, linked to the provided canvas.
        /// </summary>
        /// <param name="canvas"></param>
        public App(GameObject canvas)
        {
            _canvas = canvas;
        }

        /// <summary>
        /// Changes the screen to the provided type, so long as the screen appears in the right prefab folder and has a component deriving from ScreenAbstract attached.
        /// </summary>
        /// <typeparam name="T">The type of screen to load.</typeparam>
        public void ChangeScreen<T>() where T : ScreenAbstract
        {
            if (currentScreen != null) Object.Destroy(currentScreen.gameObject);
            currentScreen = Object.Instantiate(Resources.Load<GameObject>($"Prefabs/Screen/{typeof(T).Name}"), _canvas.transform).GetComponent<T>();
            if (currentScreen == null) Debug.LogError($"App - ChangeScreen(): Valid screen of type {typeof(T).Name} was not found in screen library.");
            else currentScreen._app = this;
        }
    }
}